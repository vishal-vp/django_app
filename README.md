# django_app

## Setup
1. Install python 3.7.1 and pipenv (User pyenv to install python 3.7.1).
2. Run `pipenv shell`.
3. Change to webapp directory.
   `cd webapp`
4. Run migrations
   `python manage.py makemigrations`
   `python manage.py migrate`
5. Create a superuser (admin user).
   `python manage.py createsuperuser`. Enter details when asked.
6. Run development server
   `python manage.py runserver`
7. You can view the website by visiting `localhost:8000` on the browser.
