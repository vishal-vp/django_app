from django.contrib.auth import login, authenticate
from main.forms import SignUpForm
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

from main.forms import PaymentForm


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'main/signup.html', {'form': form})


class HomeView(LoginRequiredMixin, View):
    login_url = '/'


    def get(self, request):
        form = PaymentForm()
        return render(request, 'main/home.html', {'form': form})

    def post(self, request):
        form = PaymentForm(request.POST)
        if form.is_valid():
            payment = form.save(commit=False)
            payment.paid_by = request.user
            payment.save()
            return render(request, 'main/payment_success.html')
        else:
            return render(request, 'main/home.html', {'form': form})
