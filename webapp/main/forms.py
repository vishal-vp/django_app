from django import forms
from django.contrib.auth.forms import UserCreationForm

from django.forms import ModelForm
from main.models import Payment
from main.models import User


class PaymentForm(ModelForm):
    class Meta:
        model = Payment
        fields = ['mode', 'detail', 'amount', 'paid_to']


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('email', 'username', 'password1', 'password2', )
