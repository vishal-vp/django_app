from django.contrib import admin

from main.models import Payment
from main.models import User

# Register your models here.
admin.site.register(Payment)
admin.site.register(User)
